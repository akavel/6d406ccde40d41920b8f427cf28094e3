self: super:

let
  lgo_ = { stdenv, fetchgit, myGoPackage, makeWrapper,
    golang-runewidth, golang-zmq4, golang-liner, golang-sys, golang-glog }:
      myGoPackage rec {
        name = "github.com-yunabe-lgo";
        goPackagePath = "github.com/yunabe/lgo";
        src = fetchgit {
          url = "https://github.com/yunabe/lgo";
          rev = "f57058f86ecf6d08fffc1d3cf6fecdf391ce7e02";
          sha256 = "1dgq1nra2qdvgr1wjrn09b3q9klnhf6dv9zpdwai1mklc96z3qk6";
        };
        buildInputs = [ makeWrapper
          golang-runewidth golang-zmq4 golang-liner golang-sys golang-glog ];
        postInstall = ''
          wrapProgram $bin/bin/lgo \
            --prefix PATH : "$PATH" \
            --prefix GOPATH : $lib:${
              builtins.concatStringsSep ":"
                (map (x: x.lib)
                  (builtins.filter (x: x ? goPackagePath)
                    buildInputs))
            }
        '';
      };

  sys_ = { stdenv, go, fetchgit, myGoPackage }: myGoPackage {
    name = "golang.org-x-sys";
    goPackagePath = "golang.org/x/sys";
    src = fetchgit {
      url = "https://go.googlesource.com/sys";
      rev = "4497e2df6f9e69048a54498c7affbbec3294ad47";
      sha256 = "028qmbfmy84pl7wmjgvrv1x7x7nzv3qr9w7vcnrcparr43k7415s";
    };
  };

  zmq4_ = { stdenv, go, fetchgit, myGoPackage, czmq, pkgconfig }: myGoPackage {
    name = "github.com-pebbe-zmq4";
    goPackagePath = "github.com/pebbe/zmq4";
    subPackages = [ "." ];
    propagatedBuildInputs = [ czmq pkgconfig ];
    src = fetchgit {
      url = "https://github.com/pebbe/zmq4";
      rev = "3515f4e6f439e167f92f2b99a9497cf5ea8e3cea";
      sha256 = "12k5272f1axx93hf9nj2ni4xhcjwg7g9l2rfw4kg4zwknx40mzbw";
    };
  };

  liner_ = { stdenv, go, fetchgit, myGoPackage, golang-runewidth }: myGoPackage {
    name = "github.com-peterh-liner";
    goPackagePath = "github.com/peterh/liner";
    buildInputs = [ golang-runewidth ];
    src = fetchgit {
      url = "https://github.com/peterh/liner";
      rev = "5a0dfa99e2aa1d433a9642e863da51402e609376";
      sha256 = "14rww75fn33mb3w87gd547gv7zr3klvgzxcpbaxd6vjls18wh831";
    };
  };

  runewidth_ = { stdenv, go, fetchgit, myGoPackage }: myGoPackage {
    name = "github.com-mattn-go-runewidth";
    goPackagePath = "github.com/mattn/go-runewidth";
    src = fetchgit {
      url = "https://github.com/mattn/go-runewidth";
      rev = "ce7b0b5c7b45a81508558cd1dba6bb1e4ddb51bb";
      sha256 = "0lc39b6xrxv7h3v3y1kgz49cgi5qxwlygs715aam6ba35m48yi7g";
    };
  };

  glog_ = { stdenv, fetchgit, myGoPackage }: myGoPackage {
    name = "github.com-golang-glog";
    goPackagePath = "github.com/golang/glog";
    src = fetchgit {
      url = "https://github.com/golang/glog";
      rev = "23def4e6c14b4da8ac2ed8007337bc5eb5007998";
      sha256 = "0jb2834rw5sykfr937fxi8hxi2zy80sj2bdn9b3jb4b26ksqng30";
    };
  };

  myGoPackage_ = { stdenv, go }:
    { goPackagePath, src, name, buildInputs ? [], ... } @ args: stdenv.mkDerivation ( args // {
      inherit name goPackagePath src;

      buildInputs = [ go ] ++ buildInputs;

      configurePhase = ''
        runHook preConfigure

        export GOPATH=${
          builtins.concatStringsSep ":"
            (map (x: x.lib)
              (builtins.filter (x: x ? goPackagePath)
                buildInputs))
        }

        mkdir -p "$NIX_BUILD_TOP/src/$(dirname "$goPackagePath")"
        cp -r . "$NIX_BUILD_TOP/src/$goPackagePath"
        export GOPATH=$NIX_BUILD_TOP:$GOPATH

        runHook postConfigure
      '';

      buildPhase = ''
        runHook preBuild

        cd "$NIX_BUILD_TOP/src/$goPackagePath"
        go install -v ${
          builtins.concatStringsSep " "
            (map (x: "${goPackagePath}/${x}")
              (args.subPackages or [ "..." ]))
        }

        runHook postBuild
      '';

      # TODO: checkPhase

      installPhase = ''
        runHook preInstall

        mkdir -p $bin
        dir="$NIX_BUILD_TOP/bin"
        [ -e "$dir" ] && cp -r $dir $bin

        mkdir -p $lib
        dir="$NIX_BUILD_TOP/pkg"
        [ -e "$dir" ] && cp -r $dir $lib \
          && cp -r "$NIX_BUILD_TOP/src" $lib

        runHook postInstall
      '';

      # What's the importance of "out" for propagatedBuildInputs ?
      outputs = [ "bin" "lib" "out" ];
    } );

in {
  lgo = self.callPackage lgo_ {};

  golang-sys = self.callPackage sys_ {};
  golang-zmq4 = self.callPackage zmq4_ {};
  golang-liner = self.callPackage liner_ {};
  golang-runewidth = self.callPackage runewidth_ {};
  golang-glog = self.callPackage glog_ {};

  myGoPackage = self.callPackage myGoPackage_ {};
}
